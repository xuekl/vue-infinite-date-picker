import Vue from 'vue'
import App from './App.vue'
// import VueInfiniteDatePicker from  '../packages'

Vue.config.productionTip = false

// Vue.use(VueInfiniteDatePicker)

new Vue({
  render: h => h(App),
}).$mount('#app')
