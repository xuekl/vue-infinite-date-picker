# vue-infinite-date-picker

## Project setup
```
yarn add vue-infinite-date-picker
```

### Usage and configuration
```
<div class="hello">
    <InfiniteDatePicker v-model="currentDate" :options="dateOptions" @change="dateChange"/>
 </div>

import {InfiniteDatePicker} from  'vue-infinite-date-picker'
export default {
    components: {
        InfiniteDatePicker
     },
     data () {
        return {
          currentDate: '2021-08-20 11:26',
          dateOptions: {
            step: 5,
            itemHeight: 40,
            formatter: ['YYYY', 'MM', 'DD', 'HH', 'mm'],
            type: 'datetime'
          }
        }
     }
     ...
```
![Image text](./examples/assets/demo.gif)
### dateOptions
```
{
    step: 5, //时间跨度，仅支持分钟列
    itemHeight: 40, //默认时间行高
    formatter: ['YYYY', 'MM', 'DD', 'HH', 'mm'],  //各列时间format，遵循moment.js
    type: 'datetime', //可选参数 year-month-day year-month moth-day day-hour-minute ...
}
```

### Lints and fixes files
```
yarn lint
```

### Customize configuration
See [Configuration Reference](https://gitee.com/xuekl/vue-infinite-date-picker).
