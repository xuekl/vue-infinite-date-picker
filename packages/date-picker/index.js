import DatePanel from './DatePanel'

DatePanel.install = function (Vue) {
  Vue.component(DatePanel.name, DatePanel)
}

export default DatePanel
