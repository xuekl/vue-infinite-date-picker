// js节流函数
export function antiJieLiu (timeout = 0) {
  var time
  return function(callback){
    if(time)
    {
      return
    }
    time = setTimeout(() => {
      clearTimeout(time);
      time = 0
      callback && callback()
    }, timeout);
  }
}

export function animate(ele,target) {
  clearInterval(ele.timer); //清楚历史定时器
  ele.timer = setInterval(function () {
    //获取步长 确定移动方向(正负值) 步长应该是越来越小的，缓动的算法。
    var step = (target - ele.scrollTop) / 5;
    //对步长进行二次加工(大于0向上取整,小于0项下取整)
    step = step > 0 ? Math.ceil(step) : Math.floor(step);
    //动画原理： 目标位置 = 当前位置 + 步长
    ele.scrollTop = ele.scrollTop + step;
    //检测缓动动画有没有停止
    if(Math.abs(target- ele.scrollTop)<= Math.abs(step)){
      ele.scrollTop = target; //直接移动指定位置
      clearInterval(ele.timer);
    }
  },30);
}
