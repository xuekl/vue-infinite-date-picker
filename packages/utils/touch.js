class Touch {
  Y; // 手指进入时屏幕横向坐标
  mY; // 手指移动时屏幕横向坐标
  nY; // 手指移动时屏幕横向坐标与进入时的差值
  timer; // 长按事件定时器
  isLong = false; // 长按标识
  constructor (props) {
    this.props = props
    this.touchDom = props.touchDom
    this.touchstart(this.touchDom)
    this.touchmove(this.touchDom)
    this.touchend(this.touchDom)
  }

  touchstart (textWrapper) {
    textWrapper.ontouchstart = (e) => {
      let touch = e.touches[0]
      this.Y = touch.pageY
      this.nY = 0
      this.mY = 0
      this.isLong = false
      this.timer = setTimeout(() => {
        this.isLong = true
      }, 300)
      this.props.start && this.props.start(e, this.Y)
    }
  }

  touchmove (textWrapper) {
    textWrapper.ontouchmove = (e) => {
      let touch = e.touches[0]
      this.mY = touch.pageY
      this.nY = Math.ceil(this.mY - this.Y)
      this.props.move && this.props.move(this.nY)
    }
  }

  touchend (textWrapper) {
    textWrapper.ontouchend = () => {
      let absNY = Math.abs(this.nY)
      clearTimeout(this.timer)
      const params = {
        absNY,
        Y: this.Y,
        mY: this.mY,
        nY: this.nY,
        isLong: this.isLong
      }
      this.props.end && this.props.end(params)
    }
  }
}

export default Touch
